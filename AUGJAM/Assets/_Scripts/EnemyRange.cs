﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour
{
    public float maxHit = 4f;
    public float level;
    CapsuleCollider range;

    void Awake()
    {
        level = Random.Range(1, 4); // maybe adjust?
        range = this.GetComponent<CapsuleCollider>();
    }


    private void OnTriggerStay(Collider other)
    {
        if((other.tag == "Player") && (!PlayerScript.safer))
        {
            // Increases Danger Meter for the Game
            PlayerScript.danger += Mathf.Abs((level * (maxHit - 
                Vector3.Distance(this.transform.position,
                other.transform.position))) * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(PlayerScript.danger > (level * 20f))
            {
                PlayerScript.danger += (level * 10);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class PlayerScript : MonoBehaviour
{
    public static bool safer;
    float timer;
    float timeStart = 5f; // update?

    public static float danger; // attach to ui
    public static float score;  // attach to ui
    public float threshold;     // max danger

    public AudioClip steps;
    AudioSource source;
    public AudioSource goodsource;

    public UnityEngine.UI.Slider slide;
    public TextMeshProUGUI sTxt;
    
    void Start()
    {
        score = 0f;
        danger = 0f;
        timer = timeStart;
        source = GetComponent<AudioSource>();
        slide.maxValue = threshold;
    }

    void Update()
    {
        // updates the slider
        slide.value = danger;

        // update score
        sTxt.text = string.Format("Score: {0}", score);

        //Debug.Log(danger);
        if(danger >= threshold)
        {
            // if "danger" level is
            // too high, ends game.
            GameOver();
        }

        if (safer)
        {
            // invincibility starts
            timer -= Time.deltaTime;

            if(timer <= 0)
            {
                // invincibility ends.
                safer = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Sani")
        {
            // danger levels decrease
            danger -= (danger * .2f);
            Destroy(other.gameObject);
            goodsource.Play();
        }
        else if(other.tag == "Facemask")
        {
            // temp. invincibility.
            timer = timeStart;
            safer = true;
            Destroy(other.gameObject);
            goodsource.Play();
        }
        else if(other.tag.Contains("Enemy"))
        {
            Debug.Log("Enemy Hit");
            EnemyScript es = other.gameObject.GetComponent<EnemyScript>();
            // plays sound
            es.Oops();
            // doesn't count in the score
            es.hit = true;
            // falls down
            other.attachedRigidbody.constraints = RigidbodyConstraints.None;
        }
    }

    public void GameOver()
    {
        score = 0;
        danger = 0;
        SceneManager.LoadScene("GameOver");
        // ends the game.
    }
}

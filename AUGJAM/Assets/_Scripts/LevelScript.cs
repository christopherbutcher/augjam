﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;

public class LevelScript : MonoBehaviour
{
    public GameObject[] tiles = new GameObject[] { }; // for multiple ground tiles.
    public GameObject[] enemies = new GameObject[] { }; // for all enemy prefabs
    public GameObject[] powerups = new GameObject[] { }; // for mask & bottle

    public static float scrollSpeed;

    void Start()
    {
        // place enemies
        EnemyPlacement((Convert.ToInt32(PlayerScript.score) / 100) * 5);
        // place powerups
        HelpingHand();
        Debug.Log(transform.position);
    }


    void FixedUpdate()
    {
        if(scrollSpeed < 0.05f)
        {
            scrollSpeed = 0.05f;
        }

        if (Vector3.Distance(transform.position, 
            new Vector3(transform.position.x, transform.position.y, -107f)) > 0)
        {
            //Debug.Log("Currently Moving!");
            transform.position = Vector3.MoveTowards(this.transform.position,
            new Vector3(transform.position.x, transform.position.y, -107), scrollSpeed);
        }
        else
        {
            //Debug.Log("Currently Destroying!!");
            int r = UnityEngine.Random.Range(0, tiles.Length);
            // this way, if the prefab's transform is not Vector3.zero,
            //           it will still spawn in the right spot (hopefully)

            GameObject t = Instantiate(tiles[r], 
                new Vector3(tiles[r].transform.position.x, tiles[r].transform.position.y,
                tiles[r].transform.position.z + 214f), tiles[r].transform.rotation);
            //t.GetComponent<LevelScript>().scrollSpeed = scrollSpeed;
            Destroy(this.gameObject);
        }
        
    }

    void EnemyPlacement(int e)
    {
        // This method places Enemies on the Environment Tile
        if (e <= 0)
        {
            for(int i = 0; i < 5; i++)
            {
                GameObject enemy =
                Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Length)],
                new Vector3(UnityEngine.Random.Range(-7.5f, 7.5f), .25f,
                UnityEngine.Random.Range(
                        (transform.position.z - 53.5f), (53.5f + transform.position.z))),
                Quaternion.Euler(0, UnityEngine.Random.Range(0, 360), 0));
                enemy.transform.SetParent(this.gameObject.transform);
            }
        }
        else
        {
            for(int i = 0; i <= e; i++)
            {
                GameObject enemy = 
                    Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Length)],
                    new Vector3(UnityEngine.Random.Range(-7.5f, 7.5f), .25f,
                    UnityEngine.Random.Range(
                        (transform.position.z - 53.5f), (53.5f + transform.position.z))),
                    Quaternion.Euler(0, UnityEngine.Random.Range(0, 360), 0));
                enemy.transform.SetParent(this.gameObject.transform);
            }
        }
    }

    void HelpingHand()
    {
        // creates one of the two power ups if the number is met.
        int rando = UnityEngine.Random.Range(0, 21);

        if(rando >= 20)
        {
            int r = UnityEngine.Random.Range(0, powerups.Length);
            GameObject helper =
                    Instantiate(powerups[r],
                    new Vector3(UnityEngine.Random.Range(-7.5f, 7.5f), 2f,
                    UnityEngine.Random.Range(
                        (transform.position.z - 53.5f), 
                        (53.5f + transform.position.z))),
                    powerups[r].transform.rotation);
            helper.transform.SetParent(this.gameObject.transform);
        }
    }
}

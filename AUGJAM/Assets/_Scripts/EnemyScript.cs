﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public EnemyRange rangeScript;
    public AudioClip ohno;
    public AudioClip yeah;
    public bool hit;
    public bool scored;

    void Awake()
    {
        rangeScript = GetComponentInChildren<EnemyRange>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.tag.Contains("Player Gate")) && !scored)
        {
            //Debug.Log("PLAYERGATE");
            scored = true;
            PlayerScript.score += (rangeScript.level * 20);
            LevelScript.scrollSpeed += 0.0002f;
            GetComponent<AudioSource>().PlayOneShot(yeah, Random.Range(0.5f, 1.5f));
        }
        else if(other.tag == "Player" && !hit)
        {
            //Debug.Log(Vector3.Distance(transform.position, other.transform.position));
            if(Vector3.Distance(this.transform.position, other.transform.position) <= 4.8f)
            {
                hit = true;
                Oops();
            }
        }
    }

    public void Oops()
    {
        //Debug.Log("ES: OOPS");
        GetComponent<AudioSource>().PlayOneShot(ohno, 0.25f);
    }
}
